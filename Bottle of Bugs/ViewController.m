//
//  ViewController.m
//  Bottle of Bugs
//
//  Created by Danny on 2/17/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

//setup global variables ***************************
int levelNumber;    //level number
BOOL gamePaused;    //game pause state
CGRect theSize;     //the size of the screen
BOOL useInternet;   //whether or not to use bugs from server
float frameRate = 1.0f/60.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //get user defaults
    NSString *dateKey = @"dateKey";
    NSDate *lastRead = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:dateKey];
    if(lastRead == nil) { // App first run: set up user defaults.
        NSDictionary *appDefaults  = [NSDictionary dictionaryWithObjectsAndKeys:[NSDate date], dateKey, nil];
        
        // do any other initialization you want to do here - e.g. the starting default values.
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"theLevel"];
        
        // sync the defaults to disk
        [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:dateKey];
    
    //setup initial variables
    levelNumber = 7;//(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"theLevel"];
    theSize = [self getRealSize];
    gamePaused = YES;
    useInternet = NO;
    
    //setup Level
    [self addLevel];
    
    //setup default image
    UIImageView *defaultImg = [[UIImageView alloc] initWithFrame:self.view.frame];
    defaultImg.image = [self launchImage];
    [self.view addSubview:defaultImg];
    defaultImg.userInteractionEnabled = NO;
    
    //check if internet is enabled
    NSString *urlString = @"http://www.kwikijoes.com/kJiPhPhp/insects.php";
    NSString *didGetData = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSASCIIStringEncoding error:nil];
    if(didGetData){
        useInternet = YES;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelay:3];
    [UIView setAnimationDuration:.8];
    defaultImg.alpha = 0;
    [UIView commitAnimations];
    
    [theGameLevel fadeOutOverlay:YES waitTime:5];
    
}


#pragma mark - setup functions

-(void)addLevel{
    theGameLevel = [[bugLevel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:theGameLevel];
}

#pragma mark - start over

-(void)startOver{
    levelNumber = 0;
    [[NSUserDefaults standardUserDefaults] setInteger:levelNumber forKey:@"theLevel"];
    [theGameLevel advanceLevel];
}

-(void)pauseGame{
    gamePaused = (gamePaused == YES) ? NO : YES;
}


#pragma mark - get size

-(CGRect)getRealSize{
    CGRect screenbounds = [[UIScreen mainScreen] bounds];
    if(NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1){
        CGFloat width = screenbounds.size.width;
        CGFloat height = screenbounds.size.height;
        UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
        
        if(UIInterfaceOrientationIsPortrait(interfaceOrientation)){
            screenbounds.size = CGSizeMake(width, height);
        }else if(UIInterfaceOrientationIsLandscape(interfaceOrientation)){
            screenbounds.size = CGSizeMake(height, width);
        }
    }
    
    return screenbounds;
}

#pragma mark - extra functions

//get launch image
-(UIImage *)launchImage {
    NSDictionary *dOfLaunchImage = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"Launch~iPhone6@3x.png", @"667x375", //iphone 6
                                    @"Launch~iPhone6@3x.png", @"736x414", //iphone 6+
                                    @"Launch~iPad@2x-Landscape.png", @"1024x768", //ipad
                                    @"Launch~iPhone4@2x.png", @"480x320", //iPhone 4
                                    @"Launch~iPhone5@2x.png", @"568x320", //iPhone 5
                                    nil];
    
    NSString *key = [NSString stringWithFormat:@"%ix%i", (int)theSize.size.width, (int)theSize.size.height];
    UIImage *launchImg = [UIImage imageNamed:[NSString stringWithFormat:@"%@", dOfLaunchImage[key]]];
    launchImg = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? [UIImage imageWithCGImage:[launchImg CGImage] scale:2 orientation:UIImageOrientationUp] : [UIImage imageWithCGImage:[launchImg CGImage] scale:2 orientation:UIImageOrientationRight];
    return launchImg;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
