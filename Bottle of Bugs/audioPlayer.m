//
//  audioPlayer.m
//  Bottle of Bugs
//
//  Created by Danny on 2/21/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import "audioPlayer.h"

@implementation audioPlayer

-(void)playSound:(NSString *)soundName loops:(BOOL)doesLoop vol:(float)theVol {
    //path to the sound file
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"]];
    
    // initialize the AVAudioPlayer
    APlayer = [AVAudioPlayer alloc];
    
    if(![APlayer initWithData:data error:NULL]){
        APlayer = nil;
    }else{
        [APlayer prepareToPlay];
        
        APlayer.currentTime = 0;  // reset player
        
        APlayer.numberOfLoops = (doesLoop == YES) ? -1 : 0;
        APlayer.volume = theVol;
        
        [APlayer play];
    }
}

-(void)stopSound{
    [APlayer stop];
}

@end
