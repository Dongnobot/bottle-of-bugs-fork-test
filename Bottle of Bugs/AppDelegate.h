//
//  AppDelegate.h
//  Bottle of Bugs
//
//  Created by Danny on 2/17/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;



@end

