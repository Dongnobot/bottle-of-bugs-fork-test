//
//  audioPlayer.h
//  Bottle of Bugs
//
//  Created by Danny on 2/21/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface audioPlayer : NSObject {
    AVAudioPlayer *APlayer;
}

-(void) playSound:(NSString *)soundName loops:(BOOL)doesLoop vol:(float)theVol;
-(void) stopSound;

@end
