//
//  bug.m
//  Bottle of Bugs
//
//  Created by Danny on 2/17/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import "bug.h"

#define RED 1
#define ORANGE 2
#define YELLOW 3
#define GREEN 4
#define BLUE 5
#define PURPLE 6
#define BROWN 7
#define BLACK 8
#define WHITE 9
#define PINK 10


@implementation bug

//global variables
extern int levelNumber;
extern BOOL gamePaused;
extern float frameRate;
extern CGRect theSize;
extern BOOL levelEnded;
extern int caughtBugsNum;
extern BOOL useInternet;

@synthesize delegate, bugType, isOnBoard;

-(id)initWithFrame:(CGRect)frame bugKind:(int)kind jar:(CGRect)jBounds{
    self = [super initWithFrame:frame];
    if(self){
        //setup
        
        //intial variables
        isCaught = isFlyer = NO;
        bugState = 1; //0=stopped 1=running
        speed = 1;
        bugType = kind;
        CGSize theBugSize;
        whichBug = secondTapCounter = tapCounter = bugCounter = 0;
        bugCounterTot = (bugType == GREEN) ? 30 :200;
        whichDir = 1;
        oriJarBounds = jBounds;
        
        float w = theSize.size.width/11;
        UIImage *tempBackImage;
        float heightPercent;
        aniSpeed = 0.2;
        
        //type of bug specifics
        switch (bugType) {
            case 1:
                //RED
                speed = 1.6;
                tempBackImage = [self theImageNamed:@"ladybug-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"ladybug-0.png"],
                             [self theImageNamed:@"ladybug-1.png"],
                             [self theImageNamed:@"ladybug-0.png"],
                             [self theImageNamed:@"ladybug-2.png"], nil];
                break;
                
            case 2:
                //ORANGE
                speed = 1;
                tempBackImage = [self theImageNamed:@"sunbeetle-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"sunbeetle-0.png"],
                             [self theImageNamed:@"sunbeetle-1.png"],
                             [self theImageNamed:@"sunbeetle-0.png"],
                             [self theImageNamed:@"sunbeetle-2.png"], nil];
                break;
                
            case 3:
                //YELOW
                speed = 1.5;
                isFlyer = YES;
                tempBackImage = [self theImageNamed:@"light-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"light-0.png"],
                             [self theImageNamed:@"light-1.png"],
                             [self theImageNamed:@"light-2.png"],
                             [self theImageNamed:@"light-1.png"], nil];
                break;
                
            case 4:
                //GREEN
                speed = 7;
                tempBackImage = [self theImageNamed:@"leafbug-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                break;
                
            case 5:
                //BLUE
                speed = 2;
                aniSpeed = 0.1;
                isFlyer = YES;
                tempBackImage = [self theImageNamed:@"fly-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"fly-0.png"],
                             [self theImageNamed:@"fly-1.png"], nil];
                break;
                
            case 6:
                //PURPLE
                speed = 1;
                aniSpeed = .4;
                tempBackImage = [self theImageNamed:@"beetle-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"beetle-0.png"],
                             [self theImageNamed:@"beetle-1.png"],
                             [self theImageNamed:@"beetle-0.png"],
                             [self theImageNamed:@"beetle-2.png"], nil];
                break;
                
            case 7:
                //BROWN
                speed = .9;
                tempBackImage = [self getImgFromServer:@"Spider-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self getImgFromServer:@"Spider-0.png"],
                             [self getImgFromServer:@"Spider-1.png"],
                             [self getImgFromServer:@"Spider-0.png"],
                             [self getImgFromServer:@"Spider-2.png"], nil];
                break;
                
            case 8:
                //BLACK
                speed = 1;
                tempBackImage = [self theImageNamed:@"ant-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"ant-0.png"],
                             [self theImageNamed:@"ant-1.png"],
                             [self theImageNamed:@"ant-2.png"],
                             [self theImageNamed:@"any-1.png"], nil];
                break;
                
            case 9:
                //WHITE
                speed = 1.3;
                isFlyer = YES;
                tempBackImage = [self theImageNamed:@"butterfly-0.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"butterfly-0.png"],
                             [self theImageNamed:@"butterfly-1.png"], nil];
                break;
                
            case 10:
                //PINK
                speed = 1.3;
                tempBackImage = [self theImageNamed:@"fuzz-2.png"];
                heightPercent = (tempBackImage.size.height*100)/tempBackImage.size.width;
                theBugSize = CGSizeMake(w, (w*heightPercent)/100);
                baseImg = tempBackImage;
                aniImages = [NSArray arrayWithObjects:
                             [self theImageNamed:@"fuzz-3.png"],
                             [self theImageNamed:@"fuzz-4.png"], nil];
                break;
                
            default:
                break;
        }
        
        speed = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? speed * 1.5 : speed;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, theBugSize.width, theBugSize.height);
        
        //set jarBounds
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(jBounds.size.height/5, self.frame.size.width/3, jBounds.size.height/8, self.frame.size.width/3);
        jarBounds = UIEdgeInsetsInsetRect(jBounds, contentInsets);
        
        while ( CGRectIntersectsRect(self.frame, jarBounds) ) {
            CGPoint tempPoint = [self getRandomStartEdge];
            self.frame = CGRectMake(tempPoint.x, tempPoint.y, theBugSize.width, theBugSize.height);
        }
        
        NSLog(@"%f x %f", self.frame.origin.x, self.frame.origin.y);
        
        bugBounds = theSize;
        
        //make it point towards the center of the screen when it is created
        theRotation = [self getRotationToCenter:TRUE];
        
        //add background image
        bugBack = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        bugBack.image = baseImg;
        [self addSubview:bugBack];
        
        if(bugType != GREEN){
            bugBack.animationDuration = aniSpeed;
            bugBack.animationImages = aniImages;
        }
        
        //add drop shadow
        if(!isFlyer){
            self.layer.shadowOpacity = .4;
            self.layer.shadowRadius = 4;
            self.layer.shadowOffset = CGSizeMake(1,1);
        }
        
        
        //add animation loop caller
        if(displayLinkBug == nil){
            displayLinkBug = [CADisplayLink displayLinkWithTarget:self selector:@selector(charLoop:)];
            [displayLinkBug addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
        }
        
    }
    return self;
}

-(void)charLoop:(CADisplayLink *)displayL {
    if(gamePaused || !isOnBoard) return;
    delta = displayLinkBug.duration/frameRate;
    [self updateBug];
}

#pragma mark - bug AI

-(void)updateBug{
    
    float addX, addY, xRight, xLeft, yBottom, yTop;
    
    if(levelEnded){
        if(isCaught){
            //self.transform = CGAffineTransformMakeRotation(-1.57);
            self.transform = CGAffineTransformScale(self.transform, 1, 1);
            [bugBack stopAnimating];
        }else{
            //make point away from center
            theRotation = [self getRotationToCenter:FALSE];
            
            self.transform = CGAffineTransformMakeRotation(theRotation);
            
            addX = cos(theRotation) * speed * 7;
            addY = sin(theRotation) * speed * 7;
            
            self.center = CGPointMake(self.center.x + addX, self.center.y + addY);
        }
        
        return;
    }
    
    //bug counter
    if(bugCounter >= bugCounterTot){
        if(bugState == 1){
            bugState = 0;
            bugCounter = 0;
            bugCounterTot = [self getTotalTime];
            
            //set bug specific stop controls
            if(bugType == GREEN){
                bugBack.image = [self theImageNamed:@"leafbug-0.png"];
            }else if(bugType == BLACK){
                NSArray *aniImagesStill = [NSArray arrayWithObjects:
                                           [self theImageNamed:@"ant-3.png"],
                                           [self theImageNamed:@"ant-4.png"],
                                           [self theImageNamed:@"ant-5.png"],
                                           [self theImageNamed:@"any-4.png"], nil];
                bugBack.animationImages = aniImagesStill;
            }else if(bugType == PINK){
                NSArray *aniImagesStill = [NSArray arrayWithObjects:
                                           [self theImageNamed:@"fuzz-1.png"],
                                           [self theImageNamed:@"fuzz-1.png"],
                                           [self theImageNamed:@"fuzz-still-2.png"],
                                           [self theImageNamed:@"fuzz-still-3.png"], nil];
                bugBack.animationImages = aniImagesStill;
                bugBack.animationDuration = 2.25;
            }else if(bugType == PURPLE){
                NSArray *aniImagesStill = [NSArray arrayWithObjects:
                                           [self theImageNamed:@"beetle-still-1.png"],
                                           [self theImageNamed:@"beetle-still-2.png"],
                                           [self theImageNamed:@"beetle-still-3.png"],
                                           [self theImageNamed:@"beetle-still-2.png"],
                                           [self theImageNamed:@"beetle-still-1.png"], nil];
                bugBack.animationImages = aniImagesStill;
                bugBack.animationDuration = .7;
            }
        }else{
            theRotation = (float)(arc4random() % 628)/100.0f;
            bugState = 1;
            bugCounter = 0;
            bugCounterTot = [self getTotalTime];
            bugBack.animationDuration = aniSpeed;
            
            //set bug specific go controls
            if(bugType == GREEN){
                bugBack.image = [self theImageNamed:@"leafbug-1.png"];
            }else if(bugType == BLACK || bugType == PINK || bugType == PURPLE){
                bugBack.animationImages = aniImages;
            }
        }
    }
    
    bugCounter += 1;
    
    if(bugState == 1){
        if(![bugBack isAnimating] && bugType != GREEN) [bugBack startAnimating];
        
        //add small rotation while moving
        if(arc4random() % 20 == 10 && bugType != GREEN && bugType != BLUE){
            whichDir = (arc4random() % 30 == 10) ? whichDir * -1 : whichDir;
            float amRR = .05 * (float)whichDir;
            theRotation = theRotation + amRR;
        }
        
        //check if a flier, if so don't rotate upside down
        if(isFlyer == YES){
            if(theRotation > M_PI/2 && theRotation <= M_PI*1.5){
                bugBack.transform = CGAffineTransformMakeScale(1, -1);
            }else{
                bugBack.transform = CGAffineTransformMakeScale(1, 1);
            }
        }
        
        //set rotation
        self.transform = CGAffineTransformMakeRotation(theRotation);
        
        addX = cos(theRotation) * speed;
        addY = sin(theRotation) * speed;
        
        xRight = self.frame.origin.x + self.frame.size.width;
        xLeft = self.frame.origin.x;
        yBottom = self.frame.origin.y + self.frame.size.height;
        yTop = self.frame.origin.y;
        
        float insetX = (!isCaught) ? self.frame.size.width/1.5 : 0;
        float insetY = (!isCaught) ? self.frame.size.height/1.5 : 0;
        
        if(xRight + addX > bugBounds.origin.x + bugBounds.size.width + insetX || xLeft < bugBounds.origin.x - insetX){
            theRotation += 2;
            addX *= -(10/speed);
        }
        
        if(yBottom + addY > bugBounds.origin.y + bugBounds.size.height + insetY || yTop < bugBounds.origin.y - insetY){
            theRotation += 2;
            addY *= -(10/speed);
        }
        
        if(!isCaught){
            if(CGRectIntersectsRect(self.frame, jarBounds)){
                theRotation = [self getRotationToCenter:FALSE];
                self.transform = CGAffineTransformMakeRotation(theRotation);
                addX = cos(theRotation) * speed;
                addY = sin(theRotation) * speed;
            }
        }
    
        self.center = CGPointMake(self.center.x + addX, self.center.y + addY);
        
        //if hopper do size manipulation
        if(bugType == GREEN){
            float posT = (float)bugCounterTot/2 - fabs((float)bugCounterTot/2 - bugCounter);
            float timePer = (posT * 100)/(float)bugCounterTot/2;
            float scaleFactor = (isCaught) ? 16 : 4;
            float scalePer = (bugCounterTot/scaleFactor * timePer)/100;
            self.transform = CGAffineTransformScale(self.transform, 1+scalePer, 1+scalePer);
            
            //change shadow
            float shadowPerA = (2 * timePer)/100;
            self.layer.shadowOpacity = .4 - shadowPerA;
            self.layer.shadowRadius = 4+scalePer;
            self.layer.shadowOffset = CGSizeMake(1,1*timePer);
        }
        
    }else{
        if([bugBack isAnimating] && bugType != GREEN && bugType != BLACK && bugType != PINK && bugType != PURPLE) [bugBack stopAnimating];
        
        //ant antennaes moving when still
        if(bugType == BLACK || bugType == PINK || bugType == PURPLE) [bugBack startAnimating];
        
        //leaf bug turn to leaf logic
        if(bugType == GREEN){
            int ran = arc4random() % 60;
            if(ran == 3){
                if(bugType == GREEN) bugBack.image = [self theImageNamed:@"leafbug-still.png"];
                
            }
        }
    }
    
}

#pragma mark - touch functions

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if(bugType != levelNumber){
        secondTap = [NSTimer scheduledTimerWithTimeInterval:.03 target:self selector:@selector(checkSecondTap) userInfo:nil repeats:YES];
        tapCounter += 1;
    }else{
        //jarBounds = CGRectInset(jarBounds, self.frame.size.width/4, self.frame.size.height/4);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.1];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        self.center = CGPointMake(oriJarBounds.origin.x + oriJarBounds.size.width/2, oriJarBounds.origin.y + oriJarBounds.size.height - (self.frame.size.height*1.5));
        [UIView commitAnimations];
        isCaught = YES;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(oriJarBounds.size.height/5, self.frame.size.width/6, self.frame.size.width/3, self.frame.size.width/6);
        bugBounds = UIEdgeInsetsInsetRect(oriJarBounds, contentInsets);
        self.userInteractionEnabled = NO;
        whichBug = caughtBugsNum + 1;
        [delegate updateCaughtBugsNum];
    }
}

-(void)checkSecondTap{
    if(secondTapCounter == 100){
        [secondTap invalidate];
        secondTap = nil;
        secondTapCounter = 0;
        tapCounter = 0;
    }
    secondTapCounter += 1;
    if(tapCounter == 3 && bugState == 0) {
        
        NSLog(@"Easter Egg");
        
        [secondTap invalidate];
        secondTap = nil;
        secondTapCounter = 0;
        tapCounter = 0;
    }
}

#pragma mark - get bug from server

-(UIImage *)getImgFromServer:(NSString *)theURL{
    NSString *imageUrlString = [NSString stringWithFormat:@"http://www.kwikijoes.com/kJiPhPhp/images/%@", theURL];
    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageUrlString]];
    UIImage *image = [UIImage imageWithData:imgData];
    if(image == nil) image = [UIImage imageNamed:@"sunbeetle-0.png"];
    image = [UIImage imageWithCGImage:[image CGImage] scale:2 orientation:UIImageOrientationUp];
    return image;
}

#pragma mark - extra functions

-(float)getRotationToCenter:(BOOL)pointCenter{
    float a;
    if(pointCenter == NO){
        a = self.center.y - theSize.size.height/2;
    }else{
        a = theSize.size.height/2 - self.center.y;
    }
    
    float b = self.center.x - theSize.size.width/2;
    float adjuster = (self.center.x < theSize.size.width/2) ? M_PI : 0;
    return atan(a/b) + adjuster;
}

-(CGPoint)getRandomStartEdge{
    CGPoint startPoint;
    float startX, startY;
    
    startX = (arc4random() % (int)(theSize.size.width - self.frame.size.width))+self.frame.size.width;
    startY = (arc4random() % (int)(theSize.size.height - self.frame.size.height))+self.frame.size.height;
    
    int randEdge = arc4random() % 4;
    
    switch (randEdge) {
        case 0:
            startY = 0;
            break;
            
        case 1:
            startX = theSize.size.width - self.frame.size.width;
            break;
            
        case 2:
            startY = theSize.size.height - self.frame.size.height;
            break;
            
        case 3:
            startX = 0;
            break;
            
        default:
            break;
    }
    
    return startPoint;
}

-(int)getTotalTime{
    int totalTimePerBug;
    if(bugState == 0){
        totalTimePerBug = 200;
    }else{
        totalTimePerBug = (bugType == GREEN) ? 30 : 200;
    }
    int totTime = arc4random() % totalTimePerBug;
    return totTime;
}

-(UIImage *)theImageNamed:(NSString *)imgName{
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", imgName]];
    img = [UIImage imageWithCGImage:[img CGImage] scale:2 orientation:UIImageOrientationUp];
    return img;
}

-(void)removeBug{
    if(displayLinkBug != nil){
        [displayLinkBug invalidate];
        displayLinkBug = nil;
    }
    [self removeFromSuperview];
}

@end
