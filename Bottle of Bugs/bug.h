//
//  bug.h
//  Bottle of Bugs
//
//  Created by Danny on 2/17/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol bugNumDel <NSObject>

-(void)updateCaughtBugsNum;

@end

@interface bug : UIView {
    CADisplayLink *displayLinkBug;
    UIImageView *bugBack;
    float delta, theRotation, tempRot, speed, aniSpeed;
    BOOL isCaught, isFlyer;
    int bugState, whichBug, whichDir, secondTapCounter, tapCounter, bugCounter, bugCounterTot;
    CGRect bugBounds, jarBounds, oriJarBounds;
    NSTimer *secondTap;
    UIImage *baseImg;
    NSArray *aniImages;
}

@property (nonatomic) id <bugNumDel> delegate;
@property (nonatomic) int bugType;
@property (nonatomic) BOOL isOnBoard;

-(id)initWithFrame:(CGRect)frame bugKind:(int)kind jar:(CGRect)jBounds;
-(void)charLoop:(CADisplayLink *)displayL;
-(void)updateBug;
-(void)removeBug;
-(UIImage *)getImgFromServer:(NSString *)theURL;

@end
