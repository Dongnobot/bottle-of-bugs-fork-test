//
//  bugLevel.m
//  Bottle of Bugs
//
//  Created by Danny on 2/17/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import "bugLevel.h"

@implementation bugLevel

//global variables import
extern int levelNumber;
extern BOOL gamePaused;
extern CGRect theSize;
BOOL levelEnded;
int caughtBugsNum;

-(id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if(self){
        //setup
        caughtBugsNum = bugNumbers = 0;
        levelEnded = NO;
        audioPlayerA = [[audioPlayer alloc] init];
        [audioPlayerA playSound:@"opening" loops:NO vol:1];
        
        //add background image
        backG = [[UIImageView alloc] initWithFrame:self.frame];
        backG.image = [UIImage imageNamed:@"Grass.jpg"];
        [self addSubview:backG];
        
        //add jar back
        jarBack = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width/3.5, self.frame.size.height/1.3)];
        UIImage *jarImage = [UIImage imageNamed:@"Jar.png"];
        float theScale = 100/((jarBack.frame.size.width*100)/jarImage.size.width);
        jarImage = [UIImage imageWithCGImage:[jarImage CGImage] scale:theScale orientation:UIImageOrientationUp];
        jarBack.image = [jarImage resizableImageWithCapInsets:UIEdgeInsetsMake(jarImage.size.height/2, 0, jarImage.size.height/3, 0)];
        [self addSubview:jarBack];
        jarBack.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        
        //add jar top
        jarTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, jarBack.frame.size.width/1.519, (jarBack.frame.size.width/1.519)*.465)];
        jarTop.image = [UIImage imageNamed:[NSString stringWithFormat:@"Lid-%@.png", [self getLevelColor]]];
        [jarBack addSubview:jarTop];
        jarTop.center = CGPointMake(jarBack.frame.size.width/2, jarTop.frame.size.height/2.3);
        
        //addjarOutside
        jarOutside = [[UIImageView alloc] initWithFrame:jarBack.frame];
        jarImage = [UIImage imageNamed:@"Jar-glare.png"];
        jarImage = [UIImage imageWithCGImage:[jarImage CGImage] scale:theScale orientation:UIImageOrientationUp];
        jarOutside.image = [jarImage resizableImageWithCapInsets:UIEdgeInsetsMake(jarImage.size.height/2, 0, jarImage.size.height/3, 0)];
        [self addSubview:jarOutside];
        jarOutside.center = jarBack.center;
        
        //add number of bugs caught
        bugNumberField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, jarBack.frame.size.width, jarBack.frame.size.height/2)];
        bugNumberField.font = [UIFont fontWithName:@"Sauna-Black" size:bugNumberField.frame.size.height/1.2];
        bugNumberField.backgroundColor = [UIColor clearColor];
        bugNumberField.textColor = [UIColor blackColor];
        bugNumberField.textAlignment = NSTextAlignmentCenter;
        bugNumberField.text = [NSString stringWithFormat:@"%i", caughtBugsNum];
        bugNumberField.userInteractionEnabled = NO;
        [self addSubview:bugNumberField];
        bugNumberField.center = CGPointMake(jarBack.center.x, jarBack.center.y - jarBack.frame.size.height/70);
        
        [self addInitialBugs];
        
        [self addOverlay];
        
        //add timer for spawning bugs
        if([theLoopTimer isValid]) [theLoopTimer invalidate];
        theLoopTimer = [NSTimer scheduledTimerWithTimeInterval:.3 target:self selector:@selector(theLoop) userInfo:nil repeats:YES];
        
    }
    return self;
}

-(void)addOverlay{
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:overlay];
    
    //add back fade
    backFade = [[UIImageView alloc] initWithFrame:overlay.frame];
    backFade.image = [UIImage imageNamed:@"backFade.png"];
    [overlay addSubview:backFade];
    
    //add Color background
    UIImage *backImg = [self theImageNamed:[self getLevelColor]];
    blackBack = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, backImg.size.width, backImg.size.height)];
    blackBack.image = backImg;
    [overlay addSubview:blackBack];
    blackBack.center = CGPointMake(theSize.size.width/2, theSize.size.height/2);
    
    overlay.userInteractionEnabled = NO;
    
}

-(void)fadeOutOverlay:(BOOL)fOut waitTime:(float)t{
    if(fOut == YES){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDelay:t];
        [UIView setAnimationDuration:1];
        blackBack.alpha = 0;
        [UIView setAnimationDelay:t+.2];
        [UIView setAnimationDuration:1.2];
        overlay.alpha = 0;
        [UIView commitAnimations];
    }else{
        //set color text screen
        blackBack.image = [self theImageNamed:[self getLevelColor]];
        blackBack.alpha = 0;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDelay:5];
        [UIView setAnimationDuration:.4];
        overlay.alpha = 1;
        [UIView setAnimationDelay:5.2];
        [UIView setAnimationDuration:1];
        blackBack.alpha = 1;
        [UIView commitAnimations];
    }
}


#pragma mark - spawn loop

-(void)theLoop{
    if(gamePaused) return;
    
    //spawn bugs by moving them into view area
    if(arc4random()%10 == 3){
        bugNumbers += 1;
        
        //move bugs into view
        
    }
    
}

-(void)addInitialBugs{
    bugNumbers = 0;
    for (int a = 0; a < 11; a++) {
        bugNumbers += 1;
        
        //spawn bugs
        int randomBug = [self getRandomBug];
        float spawnX, spawnY;
        spawnX = (bugNumbers <= 5) ? (arc4random() % (int)(theSize.size.width - theSize.size.width/8))+theSize.size.width/10 : -300;
        spawnY = (bugNumbers <= 5) ? (arc4random() % (int)(theSize.size.width - theSize.size.height/8))+theSize.size.height/10 : -300;
        bug *newBug = [[bug alloc] initWithFrame:CGRectMake(spawnX, spawnY, 0, 0) bugKind:randomBug jar:jarBack.frame];
        newBug.delegate = self;
        newBug.tag = bugNumbers;
        newBug.isOnBoard = (bugNumbers <= 5) ? YES : NO;
        [self insertSubview:newBug belowSubview:jarBack];
    }
}

#pragma mark - advance to the next level

-(void)updateCaughtBugsNum{
    caughtBugsNum += 1;
    bugNumberField.text = [NSString stringWithFormat:@"%i", caughtBugsNum];
    
    if(caughtBugsNum == 5){
        levelEnded = YES;
        levelNumber = (levelNumber < 10) ? levelNumber + 1 : 1;
        [[NSUserDefaults standardUserDefaults] setInteger:levelNumber forKey:@"theLevel"];
        
        if([theLoopTimer isValid]) [theLoopTimer invalidate];
        [self fadeOutOverlay:NO waitTime:0];
        [self performSelector:@selector(advanceLevel) withObject:nil afterDelay:6.75];
    }
}

-(void)advanceLevel{
    [self fadeOutOverlay:YES waitTime:3];
    
    //remove old bugs
    for (bug *tempBug in self.subviews) {
        if ([tempBug isKindOfClass:bug.class]) {
            [tempBug removeBug];
        }
    }
    
    //update background image
    //backG.backgroundColor = [UIColor grayColor];
    jarTop.image = [UIImage imageNamed:[NSString stringWithFormat:@"Lid-%@.png", [self getLevelColor]]];
    
    //update caught bugs num
    caughtBugsNum = 0;
    levelEnded = NO;
    bugNumberField.text = [NSString stringWithFormat:@"%i", caughtBugsNum];
    
    gamePaused = NO;
    [self addInitialBugs];
    if([theLoopTimer isValid]) [theLoopTimer invalidate];
    theLoopTimer = [NSTimer scheduledTimerWithTimeInterval:.3 target:self selector:@selector(theLoop) userInfo:nil repeats:YES];
}

#pragma mark - extra functions

-(NSString *)getLevelColor{
    NSString *retCol = @"Red";
    
    switch (levelNumber) {
        case 1:
            retCol = @"Red";
            break;
            
        case 2:
            retCol = @"Orange";
            break;
            
        case 3:
            retCol = @"Yellow";
            break;
            
        case 4:
            retCol = @"Green";
            break;
            
        case 5:
            retCol = @"Blue";
            break;
            
        case 6:
            retCol = @"Purple";
            break;
            
        case 7:
            retCol = @"Brown";
            break;
            
        case 8:
            retCol = @"Black";
            break;
            
        case 9:
            retCol = @"White";
            break;
            
        case 10:
            retCol = @"Pink";
            break;
            
        default:
            break;
    }
    
    return retCol;
}

-(int)getRandomBug{
    int randomBug, totalCurrentBugs, totalBugs;
    totalBugs = totalCurrentBugs = 0;
    
    //add up total bugs and total current color bugs
    for(bug *tempBug in self.subviews){
        if([tempBug isKindOfClass:bug.class]){
            totalBugs += 1;
            if(tempBug.bugType == levelNumber) totalCurrentBugs += 1;
        }
    }
    
    //make right percentage of correct color bugs
    if(totalBugs > 0) {
        if((totalCurrentBugs*100)/totalBugs < 70){
            randomBug = levelNumber;
        }else{
            randomBug = (arc4random() % levelNumber) + 1;
        }
    }else{
        randomBug = (arc4random() % levelNumber) + 1;
    }
    
    return randomBug;
}

-(void)removeTimers{
    if([theLoopTimer isValid]){
        [theLoopTimer invalidate];
        theLoopTimer = nil;
    }
}

-(UIImage *)theImageNamed:(NSString *)imgName{
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", imgName]];
    float scaler = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 2 : 2.75;
    img = [UIImage imageWithCGImage:[img CGImage] scale:scaler orientation:UIImageOrientationUp];
    return img;
}

@end
