//
//  ViewController.h
//  Bottle of Bugs
//
//  Created by Danny on 2/17/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "bugLevel.h"

@interface ViewController : UIViewController {
    bugLevel *theGameLevel;
}

-(void)startOver;
-(void)pauseGame;

@end

