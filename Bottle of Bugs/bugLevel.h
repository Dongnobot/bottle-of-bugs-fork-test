//
//  bugLevel.h
//  Bottle of Bugs
//
//  Created by Danny on 2/17/15.
//  Copyright (c) 2015 Kwiki Joes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "bug.h"
#import "audioPlayer.h"

@interface bugLevel : UIView <bugNumDel> {
    UIImageView *jarBack, *backG, *jarTop, *jarOutside;
    UITextField *bugNumberField, *colorField;
    NSTimer *theLoopTimer;
    audioPlayer *audioPlayerA;
    UIImageView *backFade, *blackBack;
    UIView *overlay;
    int bugNumbers;
}

-(void)removeTimers;
-(void)updateCaughtBugsNum;
-(void)advanceLevel;
-(void)fadeOutOverlay:(BOOL)fOut waitTime:(float)t;

@end
